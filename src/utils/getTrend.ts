import { ArrowUp, ArrowDown, Equal } from "Components/Icons";

interface Trend {
  trend: "up" | "down" | "equal";
  Icon: typeof ArrowUp;
  operator?: "+" | "-";
  diff: number;
}

const trends = {
  up: {
    trend: "up",
    Icon: ArrowUp,
    operator: "+",
  } as Trend,
  down: {
    trend: "down",
    Icon: ArrowDown,
    operator: "-",
  } as Trend,
  equal: {
    trend: "equal",
    Icon: Equal,
  } as Trend,
};

type F_getTrend = (yesterdayRank: number, todayRank: number) => Trend;

const getTrend: F_getTrend = (yesterdayRank, todayRank) => {
  if (todayRank < yesterdayRank || yesterdayRank === 0) {
    return { ...trends.up, diff: Math.abs(todayRank - yesterdayRank) };
  } else if (todayRank > yesterdayRank) {
    return { ...trends.down, diff: Math.abs(yesterdayRank - todayRank) };
  } else {
    return { ...trends.equal, diff: 0 };
  }
};

export default getTrend;
