import type { I_Rank } from "types";

const decorateLeaderboardData = (data: I_Rank) => {
  if (data.searchResult.status === "success") {
    const filteredTop100 = data.top100.filter(
      (p) => p.todayRank < data.searchResult.players[0].todayRank
    );

    const searchResult = data.searchResult.players.map((p) => ({
      ...p,
      searchResult: true,
    }));

    return { top100: [...filteredTop100, ...searchResult] };
  } else {
    return data;
  }
};

export default decorateLeaderboardData;
