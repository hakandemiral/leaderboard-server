import { useState } from "react";
import { ToastContainer } from "react-toastify";
import { resetCss, fontFace, rootCss } from "global.styles";

import { Header, Main, Footer, HeroSection, Leaderboard } from "Components";

import "react-toastify/dist/ReactToastify.css";

const App = () => {
  resetCss();
  fontFace();
  rootCss();

  const [searchedUser, setSearchedUser] = useState("");

  return (
    <>
      <Header />
      <Main>
        <HeroSection setSearchedUser={setSearchedUser} />
        <Leaderboard searchedUser={searchedUser} />
      </Main>
      <Footer />

      <ToastContainer theme="dark" position="bottom-center" />
    </>
  );
};

export default App;
