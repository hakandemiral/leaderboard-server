import { globalCss } from "stitches.config";

import {
  NeueWoff,
  NeueWoff2,
  SatoshiVariableWoff,
  SatoshiVariableWoff2,
} from "fonts";

export const resetCss = globalCss({
  "html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, main, menu, nav, output, ruby, section, summary, time, mark, audio, video":
    {
      margin: "0",
      padding: "0",
      border: "0",
      fontSize: "100%",
      font: "inherit",
      verticalAlign: "baseline",
    },
  "article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section":
    {
      display: "block",
    },
  "*[hidden]": {
    display: "none",
  },
  body: {
    lineHeight: "1",
  },
  "ol, ul": {
    listStyle: "none",
  },
  "blockquote, q": {
    quotes: "none",
  },
  "blockquote:before, blockquote:after, q:before, q:after": {
    content: "",
    // @ts-ignore
    // eslint-disable-next-line no-dupe-keys
    content: "none",
  },
  table: {
    borderSpacing: "0",
  },
});

export const fontFace = globalCss({
  "@font-face": [
    {
      fontFamily: "Satoshi Variable",
      src: `local('Satoshi Variable Bold'), 
      local('SatoshiVariable-Bold'), 
      url(${SatoshiVariableWoff2}) format('woff2'), 
      url(${SatoshiVariableWoff}) format('woff')`,
      fontWeight: "bold",
      fontStyle: "normal",
      fontDisplay: "swap",
    },
    {
      fontFamily: "PP Neue Machina",
      src: `local('Neue Machina Ultrabold'), 
      local('NeueMachina-Ultrabold'), 
      url(${NeueWoff2}) format('woff2'),
      url(${NeueWoff}) format('woff')`,
      fontWeight: 800,
      fontStyle: "normal",
      fontDisplay: "swap",
    },
  ],
});

export const rootCss = globalCss({
  ":root": {
    backgroundColor: "$violet1",
  },
});
