import type * as StitchesType from "@stitches/react";

import { createStitches } from "@stitches/react";
import {
  mauveDark,
  violetDark,
  redDark,
  greenDark,
  yellowDark,
} from "@radix-ui/colors";

export const {
  styled,
  css,
  globalCss,
  keyframes,
  getCssText,
  theme,
  createTheme,
  config,
} = createStitches({
  theme: {
    colors: {
      ...mauveDark,
      ...violetDark,
      ...redDark,
      ...greenDark,
      ...yellowDark,
    },
  },
  utils: {
    marginX: (value: StitchesType.ScaleValue<"space">) => ({
      marginLeft: value,
      marginRight: value,
    }),
    marginY: (value: StitchesType.ScaleValue<"space">) => ({
      marginTop: value,
      marginBottom: value,
    }),
    paddingX: (value: StitchesType.ScaleValue<"space">) => ({
      paddingLeft: value,
      paddingRight: value,
    }),
    paddingY: (value: StitchesType.ScaleValue<"space">) => ({
      paddingTop: value,
      paddingBottom: value,
    }),
    lineLimit: (limit: StitchesType.PropertyValue<"lineClamp">) => ({
      overflow: "hidden",
      display: "-webkit-box",
      "-webkit-line-clamp": limit,
      "-webkit-box-orient": "vertical",
    }),
  },
});
