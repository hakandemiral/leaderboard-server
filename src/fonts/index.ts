export { default as NeueWoff } from "./NeueMachina-Ultrabold.woff";
export { default as NeueWoff2 } from "./NeueMachina-Ultrabold.woff2";
export { default as SatoshiVariableWoff } from "./SatoshiVariable-Bold.woff";
export { default as SatoshiVariableWoff2 } from "./SatoshiVariable-Bold.woff2";
