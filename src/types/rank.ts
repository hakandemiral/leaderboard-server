interface Player {
  user: {
    _id: string;
    country: string;
    userName: string;
  };
  gold: number;
  todayRank: number;
  yesterdayRank: number;
  searchResult?: boolean;
}

interface I_Rank {
  top100: Player[];
  searchResult: {
    searchedPlayer: string;
    status: "success" | "no need data" | "user not found";
    players: Player[];
  };
}

export default I_Rank;
