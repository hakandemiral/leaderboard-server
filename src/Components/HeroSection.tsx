import { styled } from "stitches.config";
import SearchBar from "./SearchBar";

type HeroSectionProps = {
  setSearchedUser: React.Dispatch<React.SetStateAction<string>>;
};

const HeroSection = ({ setSearchedUser }: HeroSectionProps): JSX.Element => {
  return (
    <Wrapper>
      <PageTitle>Leaderboard</PageTitle>
      <SearchBar
        setSearch={(userName) => {
          setSearchedUser(userName);
        }}
      />
    </Wrapper>
  );
};

export default HeroSection;

const Wrapper = styled("section", {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  gap: 16,
  margin: "80px auto",
  maxWidth: 300,
  zIndex: 3,
});

const PageTitle = styled("h1", {
  fontFamily: "PP Neue Machina",
  fontWeight: 800,
  fontSize: "40px",
  lineHeight: "56px",
  color: "$violet12",
});
