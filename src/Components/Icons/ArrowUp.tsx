import { SVGProps } from "react";

const ArrowUp = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={12}
    height={12}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M6.667 3.219v8.114H5.333V3.22L1.757 6.795l-.942-.943L6 .667l5.185 5.185-.942.943-3.576-3.576Z"
      fill="#30A46C"
    />
  </svg>
);

export default ArrowUp;
