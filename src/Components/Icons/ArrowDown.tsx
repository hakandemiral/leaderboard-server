import { SVGProps } from "react";

const ArrowDown = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={12}
    height={12}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="m6.667 8.781 3.576-3.576.942.943L6 11.333.815 6.148l.942-.943 3.576 3.576V.667h1.334V8.78Z"
      fill="#AA2429"
    />
  </svg>
);

export default ArrowDown;
