import { SVGProps } from "react";

const Equal = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={12}
    height={7}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M0 .333h12v1.334H0V.333ZM0 5h12v1.333H0V5Z" fill="#F5D90A" />
  </svg>
);

export default Equal;
