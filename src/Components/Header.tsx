import { styled } from "stitches.config";

import PanteonLogo from "assets/images/panteonLogo.png";

const Header = () => {
  return (
    <Wrapper>
      <Logo src={PanteonLogo} alt="Panteon Games logo" />
    </Wrapper>
  );
};

export default Header;

const Wrapper = styled("header", {
  height: 88,
  backgroundColor: "$violet1",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
});

const Logo = styled("img", {
  height: 32,
  userSelect: "none",
});
