import { useEffect } from "react";
import { styled } from "stitches.config";
import useSWR from "swr";
import { toast } from "react-toastify";

import {
  Link,
  Button,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";

import { endpoints } from "constant";

import Table from "./Table";
import { decorateLeaderboardData } from "utils";

import type { I_Rank } from "types";

type LeaderboardProps = {
  searchedUser?: string;
};

const fetcher = (url: string) => fetch(url).then((r) => r.json());

const Leaderboard = ({ searchedUser }: LeaderboardProps) => {
  const url = endpoints.top100;
  const query = new URLSearchParams({
    userName: searchedUser || "",
  }).toString();

  const { data: top100, error } = useSWR<I_Rank>(`${url}?${query}`, fetcher);

  useEffect(() => {
    if (error) {
      toast("An error occured!");
    }
  }, [error]);

  useEffect(() => {
    if (top100) {
      if (searchedUser && top100?.searchResult.status === "user not found") {
        toast("Player not found!", { type: "warning" });
      }

      if (searchedUser && top100) {
        const element = document.getElementById(
          top100.searchResult.searchedPlayer
        );

        element?.scrollIntoView();
      }
    }
  }, [top100]);

  return (
    <Wrapper id="leaderboard">
      {top100 && (
        <Table data={searchedUser ? decorateLeaderboardData(top100) : top100} />
      )}
    </Wrapper>
  );
};

export default Leaderboard;

const Wrapper = styled("div", {
  margin: "auto",
  maxWidth: 1200,

  "@media screen and (max-width: 1248px)": {
    padding: "0 24px",
  },
});
