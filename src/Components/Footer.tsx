import { styled } from "stitches.config";

import panteonIco from "assets/images/panteonIco.svg";

const Footer = () => (
  <Wrapper>
    Crafted with ❤️ by{" "}
    <a
      href="https://dribbble.com/gulsumyasarr"
      rel="noreferrer"
      target="_blank"
      className="link"
    >
      G
    </a>
    &
    <a
      href="https://github.com/hakandemiral"
      rel="noreferrer"
      target="_blank"
      className="link"
    >
      H
    </a>
    for <img src={panteonIco} alt="Panteon Games Ico" width={28} />
  </Wrapper>
);

export default Footer;

const Wrapper = styled("footer", {
  fontFamily: "PP Neue Machina",
  fontWeight: 800,
  fontSize: "18px",
  lineHeight: "32px",
  padding: "30px 0",
  backgroundColor: "$violet1",
  color: "$violet12",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  gap: 8,

  ".link": {
    textDecoration: "none",
    color: "inherit",

    "&:hover": {
      color: "$yellow10",
    },
  },
});
