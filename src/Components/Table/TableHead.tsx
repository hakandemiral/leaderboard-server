import { styled } from "stitches.config";

const TableHead = () => {
  return (
    <Wrapper>
      <span>Rank</span>
      <span>Trend</span>
      <span>Player</span>
      <span>Country</span>
      <span>Gems</span>
    </Wrapper>
  );
};

export default TableHead;

const Wrapper = styled("div", {
  display: "flex",
  gap: 32,
  boxShadow: "0px 1px 8px rgba(0, 0, 0, 0.25)",
  backgroundColor: "$violet2",
  color: "$mauve9",
  fontSize: 14,
  fontWeight: 700,
  lineHeight: "20px",
  padding: "20px 0",
  borderRadius: 4,
  marginBotom: 8,

  span: {
    flex: 1,
    display: "flex",
    justifyContent: "flex-start",

    "&:first-child": {
      justifyContent: "center",
    },
  },

  "@media screen and (max-width: 768px)": {
    "& > *:nth-child(1)": {
      flex: 1,
    },
    "& > *:nth-child(2)": {
      flex: 1,
    },
    "& > *:nth-child(3)": {
      flex: 2,
    },
    "& > *:nth-child(4)": {
      flex: 2,
    },
    "& > *:nth-child(5)": {
      flex: 2,
    },
  },

  "@media screen and (max-width: 425px)": {
    gap: 8,
    fontSize: 12,
    "& > *:nth-child(1)": {
      flex: 1,
    },
    "& > *:nth-child(2)": {
      flex: 1,
    },
    "& > *:nth-child(3)": {
      flex: 2,
    },
    "& > *:nth-child(4)": {
      flex: 2,
    },
    "& > *:nth-child(5)": {
      flex: 2,
    },
  },
});
