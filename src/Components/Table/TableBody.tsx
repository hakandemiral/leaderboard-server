import { styled } from "stitches.config";

import TableRow from "./TableRow";

import { CountryCell, GemCell, PlayerCell, RankCell, TrendCell } from "./Cells";
import { I_Rank } from "types";

type TableBodyProps = {
  data: I_Rank;
};

const TableBody = ({ data }: TableBodyProps) => {
  return (
    <Wrapper>
      {data?.top100?.map((player) => (
        <TableRow
          isSearchResult={player.searchResult || false}
          id={player.user.userName}
        >
          <RankCell rank={player.todayRank} />
          <TrendCell
            yesterdayRank={player.yesterdayRank}
            todayRank={player.todayRank}
          />
          <PlayerCell name={player.user.userName} />
          <CountryCell countryCode={player.user.country} />
          <GemCell totalGem={player.gold} />
        </TableRow>
      ))}
    </Wrapper>
  );
};

export default TableBody;

const Wrapper = styled("div", {
  display: "flex",
  flexDirection: "column",
  gap: 12,
  marginTop: 8,
});
