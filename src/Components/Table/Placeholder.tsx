import { styled } from "stitches.config";

type PlaceholderProps = {
  image?: string;
  message: string;
};

const Placeholder = ({ image, message }: PlaceholderProps) => (
  <Wrapper>
    {image && <img src={image} alt="placeholder img" />}
    <div className="message">{message}</div>
  </Wrapper>
);

export default Placeholder;

const Wrapper = styled("div", {
  width: "100%",
  height: 250,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
});
