import { styled } from "stitches.config";

type TableRowProps = {
  children: React.ReactNode;
  isSearchResult: boolean;
  id: string;
};

const TableRow = ({ children, isSearchResult, id }: TableRowProps) => (
  <Wrapper id={id} searchResult={isSearchResult}>
    {children}
  </Wrapper>
);

export default TableRow;

const Wrapper = styled("div", {
  width: "100%",
  height: 48,
  backgroundColor: "$violet3",
  color: "$mauve12",
  fontWeight: 500,
  fontSize: 16,
  lineHeight: "24px",
  border: "1px solid $violet7",
  display: "flex",
  gap: 32,
  overflow: "hidden",
  boxShadow:
    "clip-path: polygon(50% 0, 100% 0, 100% 50%, 50% 100%, 0 100%, 0 50%);",

  "& > *": {
    overflow: "hidden",
    flex: 1,
  },

  "@media screen and (max-width: 768px)": {
    "& > *:nth-child(1)": {
      flex: 1,
    },
    "& > *:nth-child(2)": {
      flex: 1,
    },
    "& > *:nth-child(3)": {
      flex: 2,
    },
    "& > *:nth-child(4)": {
      flex: 2,
    },
    "& > *:nth-child(5)": {
      flex: 2,
    },
  },

  "@media screen and (max-width: 425px)": {
    gap: 8,
    fontSize: 12,
    "& > *:nth-child(1)": {
      flex: 1,
    },
    "& > *:nth-child(2)": {
      flex: 1,
    },
    "& > *:nth-child(3)": {
      flex: 2,
    },
    "& > *:nth-child(4)": {
      flex: 2,
    },
    "& > *:nth-child(5)": {
      flex: 2,
    },
  },

  variants: {
    searchResult: {
      true: {
        backgroundColor: "$red5",
      },
    },
  },
});
