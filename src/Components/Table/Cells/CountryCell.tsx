import { styled } from "stitches.config";
import { CircleFlag } from "react-circle-flags";
import { useMediaQuery } from "react-responsive";

import { countries } from "constant";

type CountryCellProps = {
  countryCode: string;
};

const CountryCell = ({ countryCode }: CountryCellProps) => {
  const country = countries.find((c) => c.code === countryCode.toUpperCase());
  const isMobile = useMediaQuery({ query: "(max-width: 425px)" });

  return (
    <Wrapper>
      <CircleFlag
        countryCode={countryCode.toLowerCase()}
        height={isMobile ? 12 : 24}
      />
      <span className="countryName">{country?.name || "Unknown"}</span>
    </Wrapper>
  );
};

export default CountryCell;

const Wrapper = styled("span", {
  display: "flex",
  maxWidth: "100%",
  alignItems: "center",
  justifyContent: "flex-start",
  gap: 12,

  "@media screen and (max-width: 425px)": {
    gap: 8,
  },

  ".countryName": {
    lineLimit: 1,
  },
});
