import { styled } from "stitches.config";
import { useMediaQuery } from "react-responsive";

import { AvatarGenerator } from "random-avatar-generator";

type PlayerCellProps = {
  name: string;
};

const generator = new AvatarGenerator();

const PlayerCell = ({ name }: PlayerCellProps) => {
  const isMobile = useMediaQuery({ query: "(max-width: 425px)" });

  return (
    <Wrapper>
      {!isMobile && (
        <img src={generator.generateRandomAvatar(name)} alt="player-avatar" />
      )}
      <span className="name">{name}</span>
    </Wrapper>
  );
};

export default PlayerCell;

const Wrapper = styled("div", {
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  gap: 12,

  img: {
    width: 24,
  },
});
