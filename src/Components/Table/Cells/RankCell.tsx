import { styled } from "stitches.config";

type RankCellProps = {
  rank: number;
};

const RankCell = ({ rank }: RankCellProps) => <Wrapper>{rank}</Wrapper>;

export default RankCell;

const Wrapper = styled("span", {
  fontFamily: "PP Neue Machina",
  fontWeight: 800,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});
