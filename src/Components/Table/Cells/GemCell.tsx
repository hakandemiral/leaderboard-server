import { styled } from "stitches.config";
import { useMediaQuery } from "react-responsive";

import gemImg from "assets/images/gem.png";

type GemCellProps = {
  totalGem: number;
};

const GemCell = ({ totalGem }: GemCellProps) => {
  const isMobile = useMediaQuery({ query: "(max-width: 425px)" });

  return (
    <Wrapper>
      {!isMobile && <img src={gemImg} alt="gem" />}
      <span className="totalGem">{totalGem}</span>
    </Wrapper>
  );
};

export default GemCell;

const Wrapper = styled("div", {
  display: "flex",
  alignItems: "center",
  // justifyContent: "center",
  justifyContent: "flex-start",
  gap: 12,
});
