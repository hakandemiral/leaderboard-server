import { styled } from "stitches.config";
import { getTrend } from "utils";
import { useMediaQuery } from "react-responsive";

type TrendCellProps = {
  yesterdayRank: number;
  todayRank: number;
};

const TrendCell = ({ yesterdayRank, todayRank }: TrendCellProps) => {
  const { trend, diff, operator, Icon } = getTrend(yesterdayRank, todayRank);
  const isMobile = useMediaQuery({ query: "(max-width: 425px)" });

  return (
    <Wrapper trend={trend}>
      {!isMobile && (
        <span className="indicator">
          <Icon />
        </span>
      )}
      <span className="diff">
        {operator} {diff}
      </span>
    </Wrapper>
  );
};

export default TrendCell;

const Wrapper = styled("div", {
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  gap: 8,

  ".indicator": {
    width: 24,
    height: 24,
    borderRadius: "50%",
    display: "grid",
    placeItems: "center",
  },

  variants: {
    trend: {
      up: {
        ".indicator": {
          backgroundColor: "rgba(48, 164, 108, 0.35)",
        },
        ".diff": {
          color: "$green9",
        },
      },
      down: {
        ".indicator": {
          backgroundColor: "rgba(170, 36, 41, 0.3)",
        },
        ".diff": {
          color: "$red9",
        },
      },
      equal: {
        ".indicator": {
          backgroundColor: "rgba(245, 217, 10, 0.3)",
        },
        ".diff": {
          color: "$yellow9",
        },
      },
    },
  },
});
