import { styled } from "stitches.config";
import TableBody from "./TableBody";
import TableHead from "./TableHead";

type TableProps = {
  data: any;
};

const Table = ({ data }: TableProps) => {
  return (
    <Wrapper>
      <TableHead />
      <TableBody data={data} />
    </Wrapper>
  );
};

export default Table;

const Wrapper = styled("section", {
  fontFamily: "Satoshi Variable",
});
