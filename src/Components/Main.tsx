import { styled } from "stitches.config";
import gridBg from "assets/images/gridBg.png";

type MainProps = {
  children?: React.ReactNode;
};

const Main = ({ children }: MainProps): JSX.Element => (
  <Wrapper>{children}</Wrapper>
);

export default Main;

const Wrapper = styled("main", {
  backgroundColor: "$violet1",
  minHeight: "calc(100vh - 180px)",
  background: `url(${gridBg}), radial-gradient(100% 100% at 50% 0%, rgba(58, 30, 136, 0.6) 0%, rgba(57, 10, 98, 0.01) 100%)`,
  backgroundRepeat: "no-repeat",
  backgroundSize: "contain",
  backgroundPosition: "top",
  position: "relative",
  overflow: "hidden",
});
