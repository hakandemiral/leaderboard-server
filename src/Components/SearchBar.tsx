import { styled } from "stitches.config";

import searchIcon from "assets/images/searchIcon.svg";

type SearchBarProps = {
  setSearch: (playerName: string) => void;
};

const SearchBar = ({ setSearch }: SearchBarProps) => {
  const handleSearch = (e: React.KeyboardEvent) => {
    if (e.key === "Enter") {
      const target = e.target as HTMLInputElement;
      setSearch(target.value);
    }
  };

  return (
    <Input
      placeholder="Type a player name and press Enter..."
      onKeyDown={handleSearch}
    />
  );
};

export default SearchBar;

const Input = styled("input", {
  display: "block",
  width: "100%",
  padding: "12px 18px 12px 44px",
  margin: "0 24px",
  fontFamily: "Satoshi Variable",
  fontWeight: 700,
  lineHeight: "24px",
  border: "1px solid $mauve4",
  borderRadius: 8,
  backgroundColor: "$mauve2",
  color: "$mauve12",
  outline: "none",
  backgroundImage: `url(${searchIcon})`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center left 18px",
  boxShadow:
    "0px 2px 4px 0px #00000040, 0px 13px 40px 0px #00000040, 0px -1px 4px 0px #FFFFFF14 inset",

  "&:hover": {
    backgroundColor: "$mauve3",
  },

  "&:focus": {
    borderColor: "$violet10",
    backgroundColor: "$mauve2",
    boxShadow:
      "0px 0px 0px 4px rgba(110, 86, 207, 0.8), 0px 13px 40px rgba(0, 0, 0, 0.25), 0px 2px 4px rgba(0, 0, 0, 0.25), inset 0px -1px 4px rgba(255, 255, 255, 0.08)",
  },

  "&:placeholder": {
    color: "$mauve10",
  },
});
