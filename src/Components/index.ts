export { default as Header } from "./Header";
export { default as Main } from "./Main";
export { default as Footer } from "./Footer";
export { default as HeroSection } from "./HeroSection";
export { default as SearchBar } from "./SearchBar";
export { default as Table } from "./Table";
export { default as Leaderboard } from "./Leaderboard";
